# Water pump sensor firmware

## Flashing

1. Build nodemcu firmware with modules: GPIO, mqtt, sjson...
2. Install 
```
pip install esptool.py
wget https://ESPlorer
```
3. Connect ESP8266 to USB port
3. Write firmware from step one on ESP8266 using esptool.py
```
esptool.py --port /dev/ttyUSB0 write_flash -fm dio 0x00000 firmware.bin
```
4. Run ESPlorer
```
java -jar ESPlorer.jar
```
5. Open in ESPlorer:
* application.lua
* config.lua
* init.lua
* setup.lua
6. Edit config.lua
7. Compile and save on ESP8266:
* application.lua
* config.lua
* setup.lua
8. Save init.lua on ESP8266