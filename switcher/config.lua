local module = {}

-- firmware version
module.VERSION = "0.0.1"

-- wifi config
module.NET_NAME = "linksys"
module.NET_PWD = "12356789"

-- mqtt config
module.HOST = "192.168.1.101"
module.PORT = 1883
module.ID = node.chipid()

-- sensor pin
module.PIN = 5

return module
