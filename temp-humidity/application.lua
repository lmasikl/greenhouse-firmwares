local module = {}
broker = nil

-- Send data to the broker
local function send_data()
    status, temp, humi, temp_dec, humi_dec = dht.read(config.PIN)
    humi_data = {
        id=config.ID,
        value=humi,
        name="humidity",
        method="store_sensor_data"
    }
    
    ok, json = pcall(sjson.encode(humi_data))
    if ok then
        broker:publish(config.ID, json, 0, 0)
    end

    temp_data = {
        id=config.ID,
        value=temp,
        name="temperature",
        method="store_sensor_data"
    }
    ok, json = pcall(sjson.encode(temp_data))
    if ok then
        broker:publish(config.ID, json, 0, 0)
    end
end

-- Sends my id to the broker for registration
local function register_myself()
    broker:subscribe(config.NAME, 0, function(conn)
        print("Successfully subscribed to data endpoint")
    end)
end

local function mqtt_start()
    broker = mqtt.Client(config.ID, 120)
    -- register message callback beforehand
    broker:on("message", function(conn, topic, data) 
      if data ~= nil then
        print(topic .. ": " .. data)
        -- do something, we have received a message
      end
    end)
    -- Connect to broker
    broker:connect(config.HOST, config.PORT, 0, 1, function(con) 
        register_myself()
        -- And then pings each TIMEOUT milliseconds
        tmr.stop(6)
        tmr.alarm(6, config.TIMEOUT, 1, send_data)
    end) 
end

function module.start()
  mqtt_start()
end

return module
