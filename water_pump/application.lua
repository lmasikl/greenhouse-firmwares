local module = {}
broker = nil

-- Sends my id to the broker for registration
local function register_myself()
    broker:subscribe(config.ID, 0, function(conn)
        print("Successfully subscribed to data endpoint")
    end)
end

local function set_state(data)
    if data.action == 1 then
        gpio.write(config.PIN, gpio.HIGH)
    elseif data.action == 0 then
        gpio.write(config.PIN, gpio.LOW)
    end
end

local function mqtt_start()
    broker = mqtt.Client(config.ID, 120)
    -- register message callback beforehand
    broker:on("message", function(conn, topic, data) 
      if data ~= nil then
        print(topic .. ": " .. data)
        decoded_data = sjson.decode(data)
        set_state(decoded_data)
      end
    end)
    -- Connect to broker
    broker:connect(config.HOST, config.PORT, 0, 1, function(con) 
        register_myself()
    end) 
end

function module.start()
  mqtt_start()
end

return module
