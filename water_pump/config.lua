local module = {}

-- wifi config
module.NET_NAME = "linksys"
module.NET_PWD = "12356789"

-- mqtt config
module.HOST = "192.168.1.101"
module.PORT = 1883
module.ID = node.chipid()

-- sensor pin
module.PIN = 4

return module
