local module = {}
broker = nil

-- Send data to the broker
local function send_data()
    value = 100 - (adc.read(0) * 100) / 1024
    voltage = adc.readvdd33()
    data = {
        sensor=config.ID,
        value=value,
        name="humidity",
        method="store_sensor_data",
        voltage=voltage
    }
    
    ok, json = pcall(sjson.encode, data)
    if ok then
        broker:publish(config.ID, json, 0, 0)
    end
end

-- Update device firmware
local function update_firmware()
    -- Try to get archive with new firmware from web
    -- Unpack archive
    -- Restart device
    node.restart()
end

-- Set new data polling timeout
local function set_timeout(timeout)
    tmr.stop(6)
    tmr.alarm(6, timeout, 1, send_data)
end

-- Do requested action
local function do_action(data)
    if data.action == 'update_firmware' then
        update_firmware()
    elseif data.action == 'set_timeout' then
        set_timeout(data.timeout)
    end
end

-- Sends my id to the broker for registration
local function register_myself()
    broker:subscribe(config.ID, 0, function(conn)
        print("Successfully subscribed to data endpoint")
    end)
end

local function mqtt_start()
    broker = mqtt.Client(config.ID, 120)
    -- register message callback beforehand
    broker:on("message", function(conn, topic, data) 
      if data ~= nil then
        print(topic .. ": " .. data)
        decoded_data = sjson.decode(data)
        do_action(decoded_data)
      end
    end)
    -- Connect to broker
    broker:connect(config.HOST, config.PORT, 0, 1, function(con) 
        register_myself()
        -- And then pings each TIMEOUT milliseconds
        set_timeout(config.TIMEOUT)
    end) 
end

function module.start()
  mqtt_start()
end

return module
