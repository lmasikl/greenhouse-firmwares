local module = {}

-- firmware version
module.VERSION = "0.0.1"

-- constants
module.ONE_SECOND = 1000
module.ONE_MINUTE = module.ONE_SECOND * 60
module.ONE_HOUR = module.ONE_MINUTE * 60

-- wifi config
module.NET_NAME = "linksys"
module.NET_PWD = "12356789"

-- mqtt config
module.HOST = "192.168.1.101"
module.PORT = 1883
module.ID = node.chipid()

-- repeat timeout
module.TIMEOUT = module.ONE_MINUTE

return module
